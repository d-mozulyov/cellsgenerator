unit XmlReaders;

// compiler options
{$if CompilerVersion >= 24}
  {$LEGACYIFEND ON}
{$ifend}
{$if CompilerVersion >= 23}
  {$define UNITSCOPENAMES}
{$ifend}
{$U-}{$V+}{$B-}{$X+}{$T+}{$P+}{$H+}{$J-}{$Z1}{$A4}
{$ifndef VER140}
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
{$endif}
{$O+}{$R-}{$I-}{$Q-}{$W-}

{.$define MODECOMPACT}

interface
uses {$ifdef UNITSCOPENAMES}
       Winapi.Windows, System.SysUtils, System.Classes, System.Variants,
       System.Generics.Collections,
     {$else}
       Windows, SysUtils, Classes, Variants, Generics.Collections,
     {$endif}
     DocumentData, Generators, XmlGenerators, Readers,
     {$ifdef UNITSCOPENAMES}Winapi.MSXML, Winapi.ActiveX{$else}MSXML, ActiveX{$endif},
     OXmlSAX, OWideSupp,
     Expat;


// native ordinal types
{$if (not Defined(FPC)) and (CompilerVersion < 22)}
type
  {$if CompilerVersion < 21}
  NativeInt = Integer;
  NativeUInt = Cardinal;
  {$ifend}
  PNativeInt = ^NativeInt;
  PNativeUInt = ^NativeUInt;
{$ifend}

type

{ TXmlDocumentReader class }

  TXmlDocumentReader = class(TDocumentReader)
  protected
    FFormatSettings: TFormatSettings;
  public
    class function GetDescription: string; override;
    class function GetDefaultFileName: string; override;
    class function GetGenerator: TGeneratorClass; override;

    constructor Create(const AFileName: string); override;
  end;


{ TMSSAX60Reader class }

  TMSSAX60Reader = class(TXmlDocumentReader, IVBSAXContentHandler)
  private
    FVBSAXXMLReader: IVBSAXXMLReader;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    function GetTypeInfoCount(out Count: Integer): HResult; stdcall;
    function GetTypeInfo(Index, LocaleID: Integer; out TypeInfo): HResult; stdcall;
    function GetIDsOfNames(const IID: TGUID; Names: Pointer; NameCount, LocaleID: Integer; DispIDs: Pointer): HResult; stdcall;
    function Invoke(DispID: Integer; const IID: TGUID; LocaleID: Integer; Flags: Word; var Params; VarResult, ExcepInfo, ArgErr: Pointer): HResult; stdcall;
    procedure _Set_documentLocator(const Param1: IVBSAXLocator); safecall;
    procedure startDocument; safecall;
    procedure endDocument; safecall;
    procedure startPrefixMapping(var strPrefix: WideString; var strURI: WideString); safecall;
    procedure endPrefixMapping(var strPrefix: WideString); safecall;
    procedure startElement(var strNamespaceURI: WideString; var strLocalName: WideString; var strQName: WideString; const oAttributes: IVBSAXAttributes); safecall;
    procedure endElement(var strNamespaceURI: WideString; var strLocalName: WideString; var strQName: WideString); safecall;
    procedure characters(var strChars: WideString); safecall;
    procedure ignorableWhitespace(var strChars: WideString); safecall;
    procedure processingInstruction(var strTarget: WideString; var strData: WideString); safecall;
    procedure skippedEntity(var strName: WideString); safecall;

    procedure InternalAddStyle(const oAttributes: IVBSAXAttributes);
  public
    constructor Create(const AFileName: string); override;
    destructor Destroy; override;
    procedure Run; override;
  end;


{ TOXmlSAXReader class }

  TOXmlSAXReader = class(TXmlDocumentReader)
  private
    FSAXParser: OXmlSAX.TSAXParser;
    procedure InternalAddStyle(const aAttributes: TSAXAttributes);
    procedure DoElementEvent(Sender: TSAXParser; const aName: OWideString; const aAttributes: TSAXAttributes);
  public
    constructor Create(const AFileName: string); override;
    destructor Destroy; override;
    procedure Run; override;
  end;


{ TExpatReader class }

  PPXMLChar = ^PXMLChar;

  TExpatReader = class(TXmlDocumentReader)
  private
    FXmlParser: Expat.TXMLParser;
    procedure InternalAddStyle(Atts: PPXMLChar);
    procedure DoElementEvent(const Name: PXMLChar; Atts: PPXMLChar); cdecl;
  public
    constructor Create(const AFileName: string); override;
    destructor Destroy; override;
    procedure Run; override;
  end;

implementation


procedure FillStyleFontOptions(var Style: TStyle; const Options: string);
var
  i: Integer;
begin
  for i := Low(Options) to High(Options) do
  case Options[i] of
      ATTR_FONT_OPTION_ITALIC: Style.Italic := True;
        ATTR_FONT_OPTION_BOLD: Style.Bold := True;
   ATTR_FONT_OPTION_STRIKEOUT: Style.StrikeOut := True;
  end;
end;

{ TXmlDocumentReader }

class function TXmlDocumentReader.GetDescription: string;
begin
  Result := Self.ClassName;
  if (Result[1] = 'T') then Delete(Result, 1, 1);

  if (Pos('Reader', Result) = Length(Result) - Length('Reader') + 1) then
    SetLength(Result, Length(Result) - Length('Reader'));
end;

class function TXmlDocumentReader.GetDefaultFileName: string;
begin
  Result := 'XmlGenerator.xml';
end;

class function TXmlDocumentReader.GetGenerator: TGeneratorClass;
begin
  Result := TXmlGenerator;
end;

constructor TXmlDocumentReader.Create(const AFileName: string);
begin
  inherited;
  FFormatSettings := FormatSettings;
  FFormatSettings.DecimalSeparator := '.';
  FFormatSettings.ShortDateFormat := 'yyyy-mm-dd';
  FFormatSettings.DateSeparator := '-';
  FFormatSettings.TimeSeparator := ':';
end;


{ TMSSAX60Reader }

constructor TMSSAX60Reader.Create(const AFileName: string);
begin
  inherited;
  FVBSAXXMLReader := CoSAXXMLReader60.Create;
  FVBSAXXMLReader.contentHandler := Self;
end;

destructor TMSSAX60Reader.Destroy;
begin
  FVBSAXXMLReader := nil;
end;

procedure TMSSAX60Reader.Run;
var
  F: TFileStream;
  Stream: IStream;
begin
  F := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  Stream := TStreamAdapter.Create(F, soOwned) as IStream;
  FVBSAXXMLReader.parse(Stream);
end;

function TMSSAX60Reader.QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
begin
  Result := E_NOTIMPL;
end;

function TMSSAX60Reader._AddRef: Integer; stdcall;
begin
  Result := 0;
end;

function TMSSAX60Reader._Release: Integer; stdcall;
begin
  Result := 0;
end;

function TMSSAX60Reader.GetTypeInfoCount(out Count: Integer): HResult; stdcall;
begin
  Result := E_NOTIMPL;
end;

function TMSSAX60Reader.GetTypeInfo(Index, LocaleID: Integer;
  out TypeInfo): HResult; stdcall;
begin
  Result := E_NOTIMPL;
end;

function TMSSAX60Reader.GetIDsOfNames(const IID: TGUID; Names: Pointer;
  NameCount, LocaleID: Integer; DispIDs: Pointer): HResult; stdcall;
begin
  Result := E_NOTIMPL;
end;

function TMSSAX60Reader.Invoke(DispID: Integer; const IID: TGUID;
  LocaleID: Integer; Flags: Word; var Params;
  VarResult, ExcepInfo, ArgErr: Pointer): HResult; stdcall;
begin
  Result := E_NOTIMPL;
end;

procedure TMSSAX60Reader._Set_documentLocator(const Param1: IVBSAXLocator); safecall;
begin
end;

procedure TMSSAX60Reader.startDocument; safecall;
begin
end;

procedure TMSSAX60Reader.endDocument; safecall;
begin
end;

procedure TMSSAX60Reader.startPrefixMapping(var strPrefix: WideString;
  var strURI: WideString); safecall;
begin
end;

procedure TMSSAX60Reader.endPrefixMapping(var strPrefix: WideString); safecall;
begin
end;

procedure TMSSAX60Reader.endElement(var strNamespaceURI: WideString;
  var strLocalName: WideString; var strQName: WideString); safecall;
begin
end;

procedure TMSSAX60Reader.characters(var strChars: WideString); safecall;
begin
end;

procedure TMSSAX60Reader.ignorableWhitespace(var strChars: WideString); safecall;
begin
end;

procedure TMSSAX60Reader.processingInstruction(var strTarget: WideString;
  var strData: WideString); safecall;
begin
end;

procedure TMSSAX60Reader.skippedEntity(var strName: WideString); safecall;
begin
end;

procedure TMSSAX60Reader.InternalAddStyle(const oAttributes: IVBSAXAttributes);
var
  i: Integer;
  AttributeName, AttributeValue: WideString;
  Style: TStyle;
begin
  Style := DefaultStyle('');

  for i := 0 to oAttributes.length - 1 do
  begin
    AttributeName := oAttributes.getLocalName(i);
    AttributeValue := oAttributes.getValue(i);

    if (AttributeName = ATTR_NAME) then
    begin
      Style.Name := AttributeValue;
    end else
    if (AttributeName = ATTR_FONT_NAME) then
    begin
      Style.FontName := AttributeValue;
    end else
    if (AttributeName = ATTR_FONT_SIZE) then
    begin
      Style.FontSize := StrToInt(AttributeValue);
    end else
    if (AttributeName = ATTR_FONT_OPTIONS) then
    begin
      FillStyleFontOptions(Style, string(AttributeValue));
    end;
  end;

  Self.AddStyle(Style);
end;

procedure TMSSAX60Reader.startElement(var strNamespaceURI: WideString;
  var strLocalName: WideString; var strQName: WideString;
  const oAttributes: IVBSAXAttributes); safecall;
var
  i: Integer;
  AttributeName, AttributeValue: WideString;
  Style: PStyle;
begin
  if (strLocalName = NODE_CELL) then
  begin
    for i := 0 to oAttributes.length - 1 do
    begin
      AttributeName := oAttributes.getLocalName(i);
      AttributeValue := oAttributes.getValue(i);

      if (AttributeName = ATTR_STYLE) then
      begin
        Style := Self.FindStyle(AttributeValue);
        if (Style.Bold = TEST_FONT_BOLD) and (Style.FontSize >= TEST_FONT_SIZE) then
          Inc(FTestInformation.FontCount);
      end else
      if (AttributeName = ATTR_STRING_VALUE) then
      begin
        if (Pos(WideString(TEST_STRING_CONST), AttributeValue) <> 0) then
          Inc(FTestInformation.StringCount);
      end else
      if (AttributeName = ATTR_DATE_VALUE) then
      begin
        if (Round(StrToDate(AttributeValue, FFormatSettings)) = TEST_DATE) then
          Inc(FTestInformation.DateCount);
      end else
      if (AttributeName = ATTR_NUMBER_VALUE) then
      begin
        FTestInformation.Summ := FTestInformation.Summ + StrToFloat(AttributeValue, FFormatSettings)
      end;
    end;
  end else
  if (strLocalName = NODE_STYLE) then
  begin
    InternalAddStyle(oAttributes);
  end;
end;


{ TOXmlSAXReader }

constructor TOXmlSAXReader.Create(const AFileName: string);
begin
  inherited;
  FSAXParser := TSAXParser.Create;
  FSAXParser.OnStartElement := DoElementEvent;
end;

destructor TOXmlSAXReader.Destroy;
begin
  FSAXParser.Free;
  inherited;
end;

procedure TOXmlSAXReader.Run;
begin
  FSAXParser.ParseFile(Self.FileName);
end;

procedure TOXmlSAXReader.InternalAddStyle(const aAttributes: TSAXAttributes);
var
  i: Integer;
  Attribute: PSAXAttribute;
  Style: TStyle;
begin
  Style := DefaultStyle('');

  for i := 0 to aAttributes.Count - 1 do
  begin
    Attribute := aAttributes[i];

    if (Attribute.NodeName = ATTR_NAME) then
    begin
      Style.Name := Attribute.NodeValue;
    end else
    if (Attribute.NodeName = ATTR_FONT_NAME) then
    begin
      Style.FontName := Attribute.NodeValue;
    end else
    if (Attribute.NodeName = ATTR_FONT_SIZE) then
    begin
      Style.FontSize := StrToInt(Attribute.NodeValue);
    end else
    if (Attribute.NodeName = ATTR_FONT_OPTIONS) then
    begin
      FillStyleFontOptions(Style, string(Attribute.NodeValue));
    end;
  end;

  Self.AddStyle(Style);
end;

procedure TOXmlSAXReader.DoElementEvent(Sender: TSAXParser;
  const aName: OWideString; const aAttributes: TSAXAttributes);
var
  i: Integer;
  Attribute: PSAXAttribute;
  Style: PStyle;
begin
  if (aName = NODE_CELL) then
  begin
    for i := 0 to aAttributes.Count - 1 do
    begin
      Attribute := aAttributes[i];

      if (Attribute.NodeName = ATTR_STYLE) then
      begin
        Style := Self.FindStyle(Attribute.NodeValue);
        if (Style.Bold = TEST_FONT_BOLD) and (Style.FontSize >= TEST_FONT_SIZE) then
          Inc(FTestInformation.FontCount);
      end else
      if (Attribute.NodeName = ATTR_STRING_VALUE) then
      begin
        if (Pos(TEST_STRING_CONST, Attribute.NodeValue) <> 0) then
          Inc(FTestInformation.StringCount);
      end else
      if (Attribute.NodeName = ATTR_DATE_VALUE) then
      begin
        if (Round(StrToDate(Attribute.NodeValue, FFormatSettings)) = TEST_DATE) then
          Inc(FTestInformation.DateCount);
      end else
      if (Attribute.NodeName = ATTR_NUMBER_VALUE) then
      begin
        FTestInformation.Summ := FTestInformation.Summ + StrToFloat(Attribute.NodeValue, FFormatSettings)
      end;
    end;
  end else
  if (aName = NODE_STYLE) then
  begin
    InternalAddStyle(aAttributes);
  end;
end;


{ TExpatReader }

constructor TExpatReader.Create(const AFileName: string);
var
  StartElementHandler: TXMLStartElementHandler;
begin
  inherited;
  FXmlParser := XMLParserCreate(nil);
  XMLSetUserData(FXmlParser, Pointer(Self));
  StartElementHandler := TXMLStartElementHandler(@TExpatReader.DoElementEvent);
  XMLSetElementHandler(FXmlParser, StartElementHandler, nil);
end;

destructor TExpatReader.Destroy;
begin
  XMLParserFree(FXmlParser);
  inherited;
end;

procedure TExpatReader.Run;
var
  F: TFileStream;
  Buffer: array[Word] of Byte;
  Size: Integer;
begin
  F := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  try
    repeat
      Size := F.Read(Buffer, SizeOf(Buffer));

      if (Size <> 0) then
      begin
        if (XML_STATUS_ERROR = XMLParse(FXmlParser, Pointer(@Buffer), Size, Ord(Size <> SizeOf(Buffer)))) then
        begin
          raise Exception.Create(XMLErrorString(XMLGetErrorCode(FXmlParser)));
        end;
      end;
    until (Size <> SizeOf(Buffer));
  finally
    F.Free;
  end;
end;

procedure TExpatReader.InternalAddStyle(Atts: PPXMLChar);
var
  AttributeName, AttributeValue: string;
  Style: TStyle;
begin
  Style := DefaultStyle('');

  while Atts^ <> nil do
  begin
    AttributeName := Atts^;
    Inc(Atts);
    AttributeValue := Atts^;
    Inc(Atts);

    if (AttributeName = ATTR_NAME) then
    begin
      Style.Name := AttributeValue;
    end else
    if (AttributeName = ATTR_FONT_NAME) then
    begin
      Style.FontName := AttributeValue;
    end else
    if (AttributeName = ATTR_FONT_SIZE) then
    begin
      Style.FontSize := StrToInt(AttributeValue);
    end else
    if (AttributeName = ATTR_FONT_OPTIONS) then
    begin
      FillStyleFontOptions(Style, string(AttributeValue));
    end;
  end;

  Self.AddStyle(Style);
end;

procedure TExpatReader.DoElementEvent(const Name: PXMLChar; Atts: PPXMLChar);
var
  ElementName: string;
  AttributeName, AttributeValue: string;
  Style: PStyle;
begin
  ElementName := Name;

  if (ElementName = NODE_CELL) then
  begin
    while Atts^ <> nil do
    begin
      AttributeName := Atts^;
      Inc(Atts);
      AttributeValue := Atts^;
      Inc(Atts);

      if (AttributeName = ATTR_STYLE) then
      begin
        Style := Self.FindStyle(AttributeValue);
        if (Style.Bold = TEST_FONT_BOLD) and (Style.FontSize >= TEST_FONT_SIZE) then
          Inc(FTestInformation.FontCount);
      end else
      if (AttributeName = ATTR_STRING_VALUE) then
      begin
        if (Pos(TEST_STRING_CONST, AttributeValue) <> 0) then
          Inc(FTestInformation.StringCount);
      end else
      if (AttributeName = ATTR_DATE_VALUE) then
      begin
        if (Round(StrToDate(AttributeValue, FFormatSettings)) = TEST_DATE) then
          Inc(FTestInformation.DateCount);
      end else
      if (AttributeName = ATTR_NUMBER_VALUE) then
      begin
        FTestInformation.Summ := FTestInformation.Summ + StrToFloat(AttributeValue, FFormatSettings)
      end;
    end;
  end else
  if (ElementName = NODE_STYLE) then
  begin
    InternalAddStyle(Atts);
  end;
end;

initialization
  CoInitialize(nil);

finalization
  CoUninitialize;

end.
