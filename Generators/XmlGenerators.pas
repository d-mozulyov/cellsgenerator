unit XmlGenerators;

// compiler options
{$if CompilerVersion >= 24}
  {$LEGACYIFEND ON}
{$ifend}
{$if CompilerVersion >= 23}
  {$define UNITSCOPENAMES}
{$ifend}
{$U-}{$V+}{$B-}{$X+}{$T+}{$P+}{$H+}{$J-}{$Z1}{$A4}
{$ifndef VER140}
  {$WARN UNSAFE_CODE OFF}
  {$WARN UNSAFE_TYPE OFF}
  {$WARN UNSAFE_CAST OFF}
{$endif}
{$O+}{$R-}{$I-}{$Q-}{$W-}

interface
uses {$ifdef UNITSCOPENAMES}
       Winapi.Windows, System.SysUtils, System.Classes, System.ZLib,
     {$else}
       Windows, SysUtils, Classes, ZLib,
     {$endif}
     DocumentData, Generators;

const
  NODE_DOCUMENT = 'document';
  NODE_STYLES = 'styles';
  NODE_STYLE = 'style';
  NODE_SHEET = 'sheet';
  NODE_ROW = 'row';
  NODE_CELL = 'c';

  ATTR_NAME = 'name';
  ATTR_FONT_NAME = 'f';
  ATTR_FONT_SIZE = 's';
  ATTR_FONT_OPTIONS = 'o';
  ATTR_FONT_OPTION_ITALIC = 'i';
  ATTR_FONT_OPTION_BOLD = 'b';
  ATTR_FONT_OPTION_STRIKEOUT = 's';

  ATTR_INDEX = 'i';
  ATTR_STYLE = 'st';
  ATTR_FUNCTION = 'f';
  ATTR_BOOLEAN_VALUE = 'b';
  ATTR_NUMBER_VALUE = 'n';
  ATTR_DATE_VALUE = 'd';
  ATTR_STRING_VALUE = 's';


type
  TXmlGenerator = class(TGenerator)
  protected
    procedure BeginDocument(const StyleCount, SheetCount: NativeUInt); override;
    procedure BeginStyles; override;
    procedure WriteStyle(const Style: TStyle; const Defaults: TStyleDefaults); override;
    procedure EndStyles; override;
    procedure BeginSheet(const Name: string); override;
    procedure BeginRow(const Index: Integer; const IndexIsDefault: Boolean); override;
    procedure WriteCell(const Cell: TCell; const Defaults: TCellDefaults); override;
    procedure EndRow; override;
    procedure EndSheet; override;
    procedure EndDocument; override;
  protected
    FStream: TStream;
    FBuffer: array[Word] of Byte;
    FBufferOffset: NativeUInt;
    FFormatSettings: TFormatSettings;

    procedure Flush;
    procedure Write(const S: UTF8String);
    procedure WriteXml(const S: string);
    procedure WriteInt(const I: Integer);
    procedure WriteDate(const D: TDateTime);
  public
    constructor Create(const AFileName: string); override;
    destructor Destroy; override;
  end;

  TCompressedXmlGenerator = class(TXmlGenerator)
  protected
    FBasicStream: TStream;
  public
    constructor Create(const AFileName: string); override;
    destructor Destroy; override;
  end;

implementation

{ TXmlGenerator }

constructor TXmlGenerator.Create(const AFileName: string);
begin
  inherited;
  FStream := TFileStream.Create(AFileName, fmCreate);
  FFormatSettings := FormatSettings;
  FFormatSettings.DecimalSeparator := '.';
end;

destructor TXmlGenerator.Destroy;
begin
  FStream.Free;
  inherited;
end;

procedure TXmlGenerator.WriteXml(const S: string);
var
  Buf: UTF8String;
begin
  (* Maybe later (ToDo) *)

  {$ifdef UNICODE}
    Buf := UTF8String(S);
  {$else}
    Buf := UTF8Encode(S);
  {$endif}

  Write(Buf);
end;

procedure TXmlGenerator.WriteInt(const I: Integer);
begin
  Write(UTF8String(IntToStr(I)));
end;

procedure TXmlGenerator.WriteDate(const D: TDateTime);
var
  Buf: UTF8String;
  Year, Month, Day: Word;
begin
  DecodeDate(D, Year, Month, Day);
  Buf := UTF8String(Format('%.4d-%.2d-%.2d', [Year, Month, Day]));

  Write(Buf);
end;

procedure TXmlGenerator.Flush;
begin
  if (FBufferOffset <> 0) then
  begin
    FStream.WriteBuffer(FBuffer, FBufferOffset);
    FBufferOffset := 0;
  end;
end;

procedure TXmlGenerator.Write(const S: UTF8String);
var
  Data: PByte;
  Margin, Size: NativeUInt;
begin
  Data := Pointer(S);
  Size := Length(S);

  if (Size <> 0) then
  repeat
    Margin := SizeOf(FBuffer) - FBufferOffset;

    if (Size <= Margin) then
    begin
      Move(Data^, FBuffer[FBufferOffset], Size);
      Inc(FBufferOffset, Size);
      Break;
    end else
    begin
      Move(Data^, FBuffer[FBufferOffset], Margin);
      Inc(FBufferOffset, Margin);
      Inc(Data, Margin);
      Dec(Size, Margin);
      Flush;
    end;
  until (False);
end;

procedure TXmlGenerator.BeginDocument(const StyleCount, SheetCount: NativeUInt);
begin
  Write('<?xml version="1.0" encoding="UTF-8" ?>');
  Write(#13#10'<document>');
end;

procedure TXmlGenerator.EndDocument;
begin
  Write(#13#10'</document>');
  Flush;
end;

procedure TXmlGenerator.BeginStyles;
begin
  Write(#13#10#9'<styles>');
end;

procedure TXmlGenerator.EndStyles;
begin
  Write(#13#10#9'</styles>');
end;

procedure TXmlGenerator.WriteStyle(const Style: TStyle; const Defaults: TStyleDefaults);
begin
  Write(#13#10#9#9'<style ');
  Write('name="');
  WriteXml(Style.Name);
  Write('" ');

  if (not Defaults.FontName) then
  begin
    Write('f="');
    WriteXml(Style.FontName);
    Write('" ');
  end;

  if (not Defaults.FontSize) then
  begin
    Write('s="');
    WriteInt(Style.FontSize);
    Write('" ');
  end;

  if (Style.Italic or Style.Bold or Style.StrikeOut) then
  begin
    Write('o="');
    if (Style.Italic) then Write('i');
    if (Style.Bold) then Write('b');
    if (Style.StrikeOut) then Write('s');
    Write('" ');
  end;

  Write('/>');
end;

procedure TXmlGenerator.BeginSheet(const Name: string);
begin
  Write(#13#10#9'<sheet name="');
  WriteXml(Name);
  Write('">');
end;

procedure TXmlGenerator.EndSheet;
begin
  Write(#13#10#9'</sheet>');
end;

procedure TXmlGenerator.BeginRow(const Index: Integer;
  const IndexIsDefault: Boolean);
begin
  Write(#13#10#9#9'<row');

  if (not IndexIsDefault) then
  begin
    Write(' i="');
    WriteInt(Index);
    Write('"');
  end;

  Write('>');
end;

procedure TXmlGenerator.EndRow;
begin
  Write(#13#10#9#9'</row>');
end;

procedure TXmlGenerator.WriteCell(const Cell: TCell; const Defaults: TCellDefaults);
const
  BOOLEAN_VALUES: array[Boolean] of UTF8String = ('0', '1');
begin
  Write(#13#10#9#9#9'<c ');

  if (not Defaults.Column) then
  begin
    Write('i="');
    WriteInt(Cell.Column);
    Write('" ');
  end;

  if (not Defaults.Style) then
  begin
    Write('st="');
    WriteXml(Cell.Style);
    Write('" ');
  end;

  if (not Defaults.Func) then
  begin
    Write('f="');
    WriteXml(Cell.Func);
    Write('" ');
  end;

  if (not Defaults.Value) then
  begin
    case TVarData(Cell.Value).VType of
      varBoolean:
      begin
        Write('b="');
        Write(BOOLEAN_VALUES[TVarData(Cell.Value).VBoolean]);
        Write('" ');
      end;
      varSmallint, varInteger, varSingle, varDouble, varCurrency,
      varShortInt, varByte, varWord, varLongWord, varInt64, $0015{varUInt64}:
      begin
        Write('n="');
        Write(UTF8String(FloatToStr(Cell.Value, FFormatSettings)));
        Write('" ');
      end;
      varDate:
      begin
        Write('d="');
        WriteDate(TVarData(Cell.Value).VDate);
        Write('" ');
      end;
    else
      // string
      Write('s="');
      WriteXml(Cell.Value);
      Write('" ');
    end;
  end;


  Write('/>');
end;


{ TCompressedXmlGenerator }

constructor TCompressedXmlGenerator.Create(const AFileName: string);
begin
  inherited;
  FBasicStream := FStream;
  FStream := TCompressionStream.Create(clDefault, FBasicStream);
end;

destructor TCompressedXmlGenerator.Destroy;
begin
  FStream.Free;
  FSTream := FBasicStream;
  inherited;
end;

end.
